import * as fs from "fs";
import * as path from "path";
import * as rl from "readline";

const SPEC_EXTENSION = ".spec";
export const GLOBAL_STANZA: string = "default";
const GLOBAL_REX: RegExp = /^#\s*(GLOBAL SETTINGS|Use the \[default)/i;
const STANZA_REX: RegExp = /^\[([^\]]+)\]/;
const SETTING_REX: RegExp = /(^[\w\-_\<\>]+\s=\s[^\r\n]+)/;
const HEADDER_REX: RegExp = /^\#+([ \S]*)/gm;

const enum SpecType {
    "Stanza",
    "Setting"
}

export class Spec {
    name: string;
    doc: string;

    constructor(name: string, doc: string = "") {
        this.name = name;
        this.doc = doc;
    }
}

export class Stanza extends Spec {
    regex: RegExp;
    settings: Setting[] = [];

    constructor(name: string, doc: string, regex: RegExp | string) {
        super(name, doc);
        this.regex = typeof regex === 'string' ? new RegExp(regex) : regex;
    }

    AddSetting(setting: Setting): void {
        this.settings.push(setting);
    }
}

export class Setting extends Spec { }

export class Conf extends Spec {
    stanzas: any = {};

    constructor(name: string, doc: string) {
        super(name, doc);
    }

    AddStanza(stanza: Stanza): void {
        this.stanzas[stanza.name] = stanza;
    }
}

const parseDoc = (doc: string): string => {
    return doc.trim().replace(HEADDER_REX, '$1');
};

export function ReadSpec(confName: string, specPath: string): Promise<Conf> {

    return new Promise((resolve, reject) => {

        try {
            if (!confName.endsWith(".conf"))
                reject(`${confName} is not a valid conf file`);

            let conf: Conf = new Conf(confName, "");

            // Load the Spec file
            let filePath = path.join(specPath, `${confName}${SPEC_EXTENSION}`);
            console.log(`splunk-linter loading_spec path="${filePath}"`);

            const readInterface = rl.createInterface({
                input: fs.createReadStream(filePath),
                output: process.stdout
            });

            let currStanza: Stanza = new Stanza(GLOBAL_STANZA, "", /^default$/);
            let lastStanza: Stanza | undefined;
            let startSection: boolean = true;
            let currSetting: Setting;
            let lastSetting: Setting | undefined;
            let currSpec: SpecType = SpecType.Stanza;
            let currDoc: string = "";
            let confDoc: string = "";

            readInterface.on('line', (line) => {
                // We may be at the top section of the file
                // lets test to see if we reached the Global section
                if (startSection) {
                    if (GLOBAL_REX.test(line)) {
                        startSection = false;
                        return;
                    }
                }
                // We are still in the `default`/`Global` section
                // lets check for stanzas or settings, if none present
                // add lines to the global stanza's doc
                // New Stanza found, lets switch and commit our current Stanza
                if (STANZA_REX.test(line)) {
                    startSection = false;
                    // Remove `[` and `]` from the start and end of the Stanza
                    let name = line.slice(1, line.length - 1);
                    // Commit any uncommited Specs
                    if (currSpec === SpecType.Setting && lastSetting !== currSetting) {
                        currStanza.AddSetting(
                            new Setting(
                                line,
                                (currStanza.name === GLOBAL_STANZA) ? `_GLOBALS_\n\n${parseDoc(currDoc)}` : parseDoc(currDoc)
                            ));
                        lastSetting = currSetting;
                    }

                    // Commit new Stanza with name, doc and StanzaRex
                    let stanzaRex = `^${name.replace(/\<\w+\>/g, '.*').replace(/\//g, '\\/')}$`;
                    let newStanza = new Stanza(
                        name,
                        parseDoc(currDoc),
                        stanzaRex);
                    conf.AddStanza(newStanza);

                    // and set new currStanza, reset currDoc
                    currStanza = newStanza;
                    lastStanza = currStanza;
                    currDoc = "";

                    currSpec = SpecType.Stanza;
                    return;
                }
                // Check if we've found a Setting, if so add it
                if (SETTING_REX.test(line)) {
                    startSection = false;

                    if (currSpec === SpecType.Stanza) {
                        currStanza.doc = parseDoc(currDoc);
                        conf.AddStanza(currStanza);
                    } else {
                        currSetting.doc = (currStanza.name === GLOBAL_STANZA) ? `_GLOBALS_\n\n${parseDoc(currDoc)}` : parseDoc(currDoc);
                        currStanza.AddSetting(currSetting);
                    }

                    let name = line;
                    let newSetting = new Setting(name, "");
                    currSetting = newSetting;

                    // Reset currs
                    lastSetting = currSetting;
                    currDoc = "";

                    currSpec = SpecType.Setting;
                    return;
                }

                // No new Stanza or Setting found, add to doc string
                if (startSection) {
                    confDoc += `${line}\n`;
                } else {
                    currDoc += `${line}\n`;
                }

            });

            // On close of file, commit the current uncommitted Stanza
            readInterface.on('close', () => {
                // Commit any uncommited Specs
                if (currSpec === SpecType.Stanza) {
                    currStanza.doc = parseDoc(currDoc);
                } else {
                    currSetting.doc = (currStanza.name === GLOBAL_STANZA) ? `_GLOBALS_\n\n${parseDoc(currDoc)}` : parseDoc(currDoc);
                    currStanza.AddSetting(currSetting);
                }

                conf.AddStanza(currStanza);
                conf.doc = parseDoc(confDoc);

                // Create completed Conf object
                resolve(conf);

            });
        } catch (err) {
            reject(err);
        }
    });

}